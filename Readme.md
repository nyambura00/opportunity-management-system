# An Opportunity Management System


```
Functionalities to be implemented:

1. Create user - with a Username & Password 

2. Login - Users can login using the created Username & Password combination 

3. Users can create accounts - i.e Safaricom and add their contact details eg.name, address,mobile 

4.Users can create an opportunity and associate it with an account e.g. Network Security Opportunity for Safaricom. The opportunity should have some characteristics like Amount, Stage (Discovery, Proposal shared, Negotiations) 

5. View existing account and opportunities

```

# Technologies Used
```
1. Python Django
2. Sqlite3
3. pytz

```

# Milestones Achieved

```
1. An admin account for the system is set.
2. The Admin can perform CRUD operations on an opportunity.
3. He/she can manually perform CRUD operations on users
4. Users can create and access their accounts.
```

# To be Achieved

```
1. Users to perform the relative system functionalities like creating, editing and viewing opportunities.
2. Created user accounts to appear in the admin dashboard.

```

# Screenshots
```
Images are in the relative repo #Downloads folder.

```

# Setting up the project
```
1. Clone the repo
    git clone https://nyambura00@bitbucket.org/nyambura00/opportunity-management-system.git 

2. Creating a virtual env (Not required, but recommended)
    cd opportunity-management-system && virtualenv venv

3. Installing the relative requirements
    source venv/bin/activate && pip install -r requirements.txt

4. Handling Migrations
    python3 manage.py migrate

Since the superuser (Admin) has been created, let's run the server locally:

5. Enter terminal command, and access the project on your browser on localhost:8000.
    python3 manage.py runserver

6. Log in with the superuser account:
    username: nyambura00
    password: 7711winnie
```
